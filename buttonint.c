#include <avr/io.h>
#include <avr/interrupt.h>
#include <lcd.h>
#include <stdio.h>
#include <util/delay.h>

#define START 0
#define STOP  1
#define RESET 2
#define TRUE  1
#define FALSE 0

#define ENABLE_CLOCK 0x0A
#define DISABLE_CLOCK 0x00

volatile int cnt = 0;
volatile int millis = 0;
volatile int secs = 0;
volatile int mins = 0;
volatile int hours = 0;

volatile int mode = START;
volatile int enable_clock = FALSE;

//with the current settings
//Refrence: 07:38:499
//Timer:    07:37:39
//need to compensate for this drift
//1 second in 7:30
//this is 2.2ms per second

int main(void)
{
	char buffer[30];
	lcd_init();
	/* Set up Port B; input on Bit 0 and output on Bit 2 */
	DDRB &= ~0x01;
	DDRB &= ~0x02;
	DDRB |= 0x04;
	DDRD |= 0x04;
	/* Turn off LED on bit 2, and no pull-up resistor on input */
	PORTB &= ~0x04;;

	cli();

	/* Button on Port B pin 0 --- route via PCINT0 */
        PCMSK0 = ((1<<PCINT0)|(1<<PCINT1));
        PCICR = 0x01;


	/*Timer for stopwatch*/
	//set prescaler to 1, and enable clock
	TCCR1B = ENABLE_CLOCK;
	TCCR1A = 0x00;
	//with this setting, the total time per millis is 20,000 ticks

	//enable interrupt 
	TIMSK1 = 0x02;

	//initialise counter
	//TCNT1 = 0;

	//initialise the comparison register
	OCR1A = 20000;


	/*Timer for PWM*/
	//set pin3 of port B to output
	//DDRB |= 0x08;
	DDRD |= 0x08;
	//set clock division and mode to /1 and PWM
	//TCCR2A = 0x83;
	TCCR2A = ((1<<COM2B1)|(1<<COM2A1)|(1<<WGM20)|(1<<WGM21));
	//TCCR2B = 0x09;
	TCCR2B = ((1<<WGM22)|(1<<CS20));

	//no interrupt
	TIMSK2 = 0x00;

	//initialise the comparison register
	OCR2A = 10; //count to 10 total
	OCR2B = 5; //50% duty cycle
	//use pinD3 as the output

	/* Globally enable interrupts */
	sei();

	while (1)
	{
		lcd_clear();
		sprintf(buffer, "%02d:%02d:%02d:%02d",hours ,mins ,secs ,millis);	
		lcd_display(buffer);
		_delay_ms(50);
	}

	return 0;
}


/*Because this timer is now dual use, it has a condition determining if the
 * stopwatch clock is being incremented. This is part of ensuring that the
 * switches are reliable in operation*/
ISR(TIMER1_COMPA_vect)
{
	cnt ++;

	if(enable_clock)
	{
		millis++;

		if(millis >=100)
		{
			secs++;
			millis = 0;
		}
		if(secs>=60)
		{
			mins++;
			secs = 0;
		}
		if(mins>=60)
		{
			hours++;
			mins = 0;	
		}
	}
}

volatile int duty = 1;
ISR(PCINT0_vect)
{
	if(cnt >= 20)
	{
		cnt = 0;
		/* We assume debouncing done by electronics, but we still
 		* implement a delay before accepting the next button press
		* due to issues with detecting multiple pin state changes on 
		a transition */
		
		//button 1 does the timer
		if (! (PINB & 0x01)) {
			/* Button pushed --- input is low. */
			
			switch(mode)//power supply (hue hue hue)
			{
				case START:
					mode = STOP;
					enable_clock = FALSE;
					break;
				case STOP:
					mode = RESET;
					millis = 0;
					secs = 0;
					mins = 0;
					break;
				case RESET:
					mode = START;
					enable_clock = TRUE;
					break;
			}
		}
		//button 2 changes the duty of the PWM output
		if(!(PINB & 0x02))
		{
			duty++;
			if(duty > 10)
			{
				duty = 0;
					
				if(PORTD&0x04)
					PORTD &= ~0x04;
				else
					PORTD |= 0x04;
			}
			OCR2B = duty;
			//reset the count to ensure that the count is below the
			//threshold
			TCNT2 = 0;
		}
	}
}
